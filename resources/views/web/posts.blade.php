@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h1>Lista de Posts</h1>
                @foreach($posts as $post)
                    <div>
                        <div class="card">
                            <div class="card header">
                                {{ $post->name }}
                            </div>
                            <div class="card-body">
                                @if($post->file)
                                    <img src="{{ $post->file }}" alt="" class="img-fluid">
                                @endif
                            </div>
                            {{ $post->excerpt }}
                            <a href="{{ route('post',$post->slug) }}" class="float-right">Leer más</a>
                            <div>

                            </div>
                        </div>
                        <br>
                    </div>
                @endforeach

                {{ $posts->render() }}

            </div>
        </div>
    </div>

@endsection
